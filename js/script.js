let createSaveButton = $("#save-button");
let createReadButton = $("#read-button");
let categoryDropdown = $("#category-dropwdown");
let createNameInput  = $("#name-textfield");
let createClearForm  = $("#clearForm-button");
let readDataTable    = $("#data-table");

createSaveButton.click(function(){
     let category = categoryDropdown.find(":selected").val();
     let name = createNameInput.val();
     let tag  = $("#rfid-dropdown :selected").text(); 

     if(name == "" || tag == ""){
          alert("Fields cannot be empty")
     }else{
          saveBtnTapped(category, name, tag);
          clearAllForms();
     }
});

createClearForm.click(function(){
      clearAllForms();
});

var config = {
     apiKey: "AIzaSyDwKmFL5evBjCMLtm_6mVEq_5VTGnFqb8k",
     authDomain: "magee-web.firebaseapp.com",
     databaseURL: "https://magee-web.firebaseio.com",
     projectId: "magee-web",
     storageBucket: "magee-web.appspot.com",
     messagingSenderId: "401241416846"
};
firebase.initializeApp(config);
var database = firebase.database();
var datasetRef = firebase.database().ref('dataset/');

function saveBtnTapped(category, name, tag) {
     firebase.database().ref('dataset/').push().set({
	      category: category,
            name: name,
            tag: tag
	});
}		

(function didLoad(){
     let url = window.location.pathname;
     let val = url.substring(url.lastIndexOf('/') + 1);
     switch(val){
          case "index.html":
               console.log("index");
               break;
          case "create.html":
               configureCategoryDropdown();
               break;
          case "read.html":
               getDataset();
               break;
     }
})();

function clearAllForms(){
      categoryDropdown.val("A")
      createNameInput.val("");
      $("#rfid-dropdown option").remove();
}

function configureCategoryDropdown(){
     for(let i=65; i<70; i++){
          $("#category-dropwdown").append($("<option>", {
               value: String.fromCharCode(i),
               text: "Category " + String.fromCharCode(i)
          }));
     }
}

function populateTableWithDataset(dataset){
     if(dataset.length > 0){
          readDataTable.find('tbody').remove();
          let allRows;
          for(let i=0; i<dataset.length; i++){
                let number = "<td>" + parseInt(i+1) + "</td>";
                let category = "<td>" + dataset[i].category + "</td>";
                let name = "<td>" + dataset[i].name + "</td>";
                let tag  = "<td>" + dataset[i].tag + "</td>";
                allRows += "<tr data-expanded='false'>" + number + category + name + tag + "</tr>";
          }
          readDataTable.append("<tbody>" + allRows + "</tbody>");
          reloadTable();
     }else{
        reloadTable();
     }
}

function getDataset(){
     datasetRef.on('value', function(snapshot) {
          let dataset = [];
          snapshot.forEach(childSnapshot => {
               let item = childSnapshot.val(); 
               item.key = childSnapshot.key;
               dataset.push(item);
          });
          populateTableWithDataset(dataset);
     });
}

function clearDataset(){
     datasetRef.remove();
     location.reload(true)
}

function reloadTable(){
      let readViewHeight  = $("#readView").height();
      let readViewWidth   = $("#readView").width();
      let size = readViewHeight > 800 ? 3 : 5;

      readDataTable.footable({
             "paging": {
                  "enabled": true,
                  "size": size
            }
      });
}
